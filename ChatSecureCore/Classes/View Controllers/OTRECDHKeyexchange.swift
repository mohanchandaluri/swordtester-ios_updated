import Foundation
import CommonCrypto
import Security
import CryptorECC
import UIKit
import nuntius
@objc open class OTRECDHKeyExchange:NSObject{
    var jsonbodystring:NSString?
    var publickeybase64encoded:String = ""
    
    
////    let attributes: [String: Any] =
////        [kSecAttrKeySizeInBits as String:      256,
////         kSecAttrKeyType as String: kSecAttrKeyTypeECSECPrimeRandom,
////         kSecPrivateKeyAttrs as String:
////
////            [kSecAttrIsPermanent as String:    false]
////    ]

    
   
    @objc open func keygeneration(Encodedkey:String)  -> NSString {
        
        let encrypt = IREncryptionService()
        let keyPair = encrypt.generateKeyPair()!
        print("Public Key: \(keyPair.publicKey)")
        print("Private Key: \(keyPair.privateKey)")
        let publickey = keyPair.base64PublicKey()
        print(publickey)
//      print(publickey)
            let botdata: [String: Any] = [ "body":[ "type":"TYPE_PUBLIC_KEY" , "data":publickey]]
            let salt = "abc123"
            var error : NSError?
            let jsonData = try! JSONSerialization.data(withJSONObject: botdata, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
        
      
          let checksumderive = jsonString + salt
        
            let checksumdata:Data = sha256(string: checksumderive)!
    
            let checksum = (checksumdata.map { String(format: "%02hhx", $0) }.joined())
            print(checksum)
        
            let bodydata:[String:Any] = [ "body":["type":"TYPE_PUBLIC_KEY","data":publickey],"checksum":checksum]
        
            let jsonbodydata = try! JSONSerialization.data(withJSONObject: bodydata, options: JSONSerialization.WritingOptions.prettyPrinted)
        
            let jsonbodystring = NSString(data: jsonbodydata, encoding: String.Encoding.utf8.rawValue)! as String
            print(jsonbodystring)
            return jsonbodystring as NSString
       
        
    }
   
    
    func sha256(string: String) -> Data? {
          let len = Int(CC_SHA256_DIGEST_LENGTH)
            let data = string.data(using:.utf8)!
            var hash = Data(count:len)

            let _ = hash.withUnsafeMutableBytes {hashBytes in
                data.withUnsafeBytes {dataBytes in
                    CC_SHA256(dataBytes, CC_LONG(data.count), hashBytes)
                }
            }
            return hash
    
}
}
    
