//
//  OTRReplymessageViewCell.h
//  ChatSecureCore
//
//  Created by apple on 27/03/20.
//  Copyright © 2020 Chris Ballinger. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OTRReplymessageViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *replytextmessage;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewreply;
@property (weak, nonatomic) IBOutlet UILabel *presentmessagetext;

@end

NS_ASSUME_NONNULL_END
